FROM ubuntu:18.04

MAINTAINER Hana, <1586606701@qq.com>

ENV DEBIAN_FRONTEND=noninteractive \
    LANG=zh_CN.UTF-8 \
    LANGUAGE=zh_CN:zh:en_US:en

# 安装依赖和代码
RUN dpkg --add-architecture i386 && \
    apt update && \
    apt upgrade -y && \
	apt install -y vnc4server \
	openbox xorg obmenu xfe wget sudo \
	git software-properties-common \
	locales language-pack-zh-han* && \
	wget -nc https://dl.winehq.org/wine-builds/winehq.key && \
	apt-key add winehq.key && \
	sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ bionic main' && \
	apt update && \
	apt install -y --install-recommends winehq-stable && \
	locale-gen